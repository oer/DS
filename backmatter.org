# Local IspellDict: en

# This file generates a bibliography slide and a slide with license
# information.  License information requires that copyright years are
# set via macro copyrightyears.

# Handling of bibliography is a hack, which requires the opening p tag
# at the end to produce valid HTML.
printbibliography:references.bib
@@html:<p>@@

#+INCLUDE: license-template.org
