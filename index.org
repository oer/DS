# Local IspellDict: en
# Copyright (C) 2017, 2018, 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC-BY-SA-4.0

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="index.css" />
#+STARTUP: showeverything
#+TITLE: Open Educational Resources (OER) for Distributed Systems
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil

* What is this?
This page collects
[[https://en.wikipedia.org/wiki/Open_educational_resources][OER]]
presentations (slides with embedded audio, available under
Section [[#presentations]], preceded by usage hints in
Section [[#hints]])
for selected lectures on Distributed Systems
at the [[https://www.uni-muenster.de/][University of Münster, Germany]].

*Warning:* Presentations here were used in summer term 2018 and winter
term 2018/2019 but *are not maintained* any longer.  They contain
course-specific information, making adoption elsewhere harder than it
should be.  For summer term 2019 and beyond, I’m separating slides
that focus on specific topics, maintained in a
[[https://gitlab.com/oer/cs][group for OER on computer science]],
from course-specific additions, maintained in a
[[https://gitlab.com/oer/oer-courses][group for OER courses]].
E.g., [[https://oer.gitlab.io/oer-courses/cacs][those presentations]]
supersede the presentations here
([[https://gitlab.com/oer/oer-courses/cacs][source material]]).

If you are teaching or learning Distributed Systems, feel free to use,
share, and adapt my presentations.  As usual for projects on GitLab,
you can open [[https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html][issues]]
to report bugs or suggest improvements, ideally
with [[https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html][merge requests]].

Presentations make use of the HTML presentation framework
[[https://revealjs.com/][reveal.js]].


* Hints on reveal.js presentations
  :PROPERTIES:
  :CUSTOM_ID: hints
  :END:
#+INCLUDE: emacs-reveal/reveal.js-hints.org

* Presentations
  :PROPERTIES:
  :CUSTOM_ID: presentations
  :END:
   *Note*: Presentations linked here are generated automatically from
   their source files.  They are updated throughout the term (and
   thereafter).  The PDF versions provided below are generated via
   LaTeX from org source files.

   - [[../oer-on-oer-infrastructure/Git-introduction.org][*Git Introduction*]] ([[../oer-on-oer-infrastructure/Git-introduction.pdf][PDF]])
   - [[file:DS01-Distributed-Systems.org][*Distributed Systems*]] ([[file:DS01-Distributed-Systems.pdf][PDF]])
   - [[file:DS02-Internet.org][*Internet*]] ([[file:DS02-Internet.pdf][PDF]])
   - [[file:DS03-Web-E-Mail.org][*Web and E-Mail*]] ([[file:DS03-Web-E-Mail.pdf][PDF]])
   - [[file:DNS.org][*Domain Name System (DNS)*]] (not part of CACS 2018) ([[file:DNS.pdf][PDF]])

   For offline work (also on mobile devices), you can
   [[https://gitlab.com/oer/DS/pipelines][download the results of the latest pipeline execution on GitLab]]
   or clone the [[https://gitlab.com/oer/DS][source repository]]
   and generate presentations yourself.

* Source code and licenses
#+MACRO: gitlablink [[https://gitlab.com/oer/DS/][$1]]
#+INCLUDE: emacs-reveal/source-code-licenses.org
